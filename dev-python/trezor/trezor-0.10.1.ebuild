EAPI=5
PYTHON_COMPAT=(python3_5)

inherit distutils-r1

DEPEND=""
RDEPEND=">=dev-python/ecdsa-0.9
	 >=dev-python/protobuf-python-3.0.0
	 >=dev-python/python-mnemonic-0.17
	 >=dev-python/cython-hidapi-0.7.99
	 >=dev-python/requests-2.4.0
	 >=dev-python/click-6.2
	 dev-python/pbkdf2"

DESCRIPTION="Client side implementation for TREZOR-compatible Bitcoin hardware wallets"
HOMEPAGE="https://github.com/trezor/python-trezor"
LICENSE="LGPL-3"
SRC_URI="mirror://pypi/t/${PN}/${P}.tar.gz"
SLOT="0"
