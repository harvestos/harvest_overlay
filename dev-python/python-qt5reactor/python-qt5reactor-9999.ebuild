EAPI=5
PYTHON_COMPAT=(python{2_7,3_4,3_5})

inherit distutils-r1 git-2 

DEPEND=""
RDEPEND=""

DESCRIPTION="Twisted Qt Integration"
HOMEPAGE="https://github.com/sunu/qt5reactor"
LICENSE="MIT"
EGIT_REPO_URI="https://github.com/sunu/qt5reactor"
SLOT="0"
KEYWORDS="~amd64"
