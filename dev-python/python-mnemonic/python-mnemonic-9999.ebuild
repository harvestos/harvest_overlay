EAPI=6
PYTHON_COMPAT=(python{2_7,3_4,3_5,3_6})

inherit distutils-r1 git-r3

DEPEND=""
RDEPEND=""

DESCRIPTION="Mnemonic code for generating deterministic keys, BIP39"
HOMEPAGE="https://github.com/trezor/python-mnemonic"
LICENSE="MIT"
EGIT_REPO_URI="https://github.com/trezor/python-mnemonic"
SLOT="0"
KEYWORDS="~amd64 ~arm ~aarch64"
