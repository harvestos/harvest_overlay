EAPI=6
PYTHON_COMPAT=(python{2_7,3_4,3_5,3_6})

inherit distutils-r1 git-r3

DEPEND=""
RDEPEND=">=dev-python/cython-0.24
		dev-libs/libusb"

DESCRIPTION="Python wrapper for the hidapi"
HOMEPAGE="https://github.com/trezor/cython-hidapi"
LICENSE="GPL-3 BSD"
EGIT_REPO_URI="https://github.com/trezor/cython-hidapi"
EGIT_SUBMODULES=( hidapi )
SLOT="0"
KEYWORDS="~amd64 ~arm ~aarch64"
