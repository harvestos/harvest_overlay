#!/bin/bash

HTP_POOL_PAL="boum.org,espiv.net,db.debian.org,epic.org,mail.riseup.net,leap.se,squat.net,tachanka.org,www.1984.is,www.eff.org,www.immerda.ch,www.privacyinternational.org,www.torproject.org"
HTP_POOL_NEUTRAL="cve.mitre.org,en.wikipedia.org,lkml.org,thepiratebay.org,www.apache.org,getfedora.org,www.democracynow.org,www.duckduckgo.com,www.gnu.org,www.kernel.org,www.mozilla.org,www.stackexchange.com,www.startpage.com,www.xkcd.com"
HTP_POOL_FOE="encrypted.google.com,github.com,login.live.com,login.yahoo.com,secure.flickr.com,tumblr.com,twitter.com,www.adobe.com,www.gandi.net,www.myspace.com,www.paypal.com,www.rackspace.com,www.sony.com"
USER="root"

  if [ -e "/usr/sbin/htpdate" ]; then
          case "$2" in
                  up)
			iptables -F
			iptables -X
			iptables -t nat -F
			iptables -t nat -X
			iptables -t mangle -F
			iptables -t mangle -X
			iptables -P INPUT ACCEPT
			iptables -P FORWARD ACCEPT
			iptables -P OUTPUT ACCEPT
			echo "nameserver 1.1.1.1" > /etc/resolv.conf
			/usr/sbin/htpdate --debug --log_file /tmp/htpdate.log --allowed_per_pool_failure_ratio 0.34 --user ${USER} --pal_pool "${HTP_POOL_PAL}" --neutral_pool "${HTP_POOL_NEUTRAL}" --foe_pool "${HTP_POOL_FOE}" > /dev/null 2>&1
                        echo "nameserver 127.0.0.1" > /etc/resolv.conf
			/etc/init.d/tor restart > /dev/null 2>&1
			iptables-restore < /var/lib/iptables/rules-save
                  ;;
                  down)
                          /etc/init.d/tor stop > /dev/null 2>&1
                  ;;
          esac
  fi