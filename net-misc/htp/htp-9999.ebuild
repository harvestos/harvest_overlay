EAPI=6

inherit git-r3

DEPEND="dev-lang/perl[ithreads]
        dev-perl/DateTime
        dev-perl/DateTime-Format-DateParse
        dev-perl/Getopt-Long-Descriptive
        dev-perl/IPC-System-Simple
        dev-perl/libwww-perl"

RDEPEND=""

DESCRIPTION="HTP date tails Time sync"
HOMEPAGE="https://git-tails.immerda.ch/htp/"
LICENSE=""
EGIT_REPO_URI="https://git-tails.immerda.ch/htp"
SLOT="0"
KEYWORDS="~amd64 ~arm ~aarch64"
IUSE="+networkmanager"
COMMON_DEPEND="networkmanager? ( net-misc/networkmanager )"


src_install() {
    dosbin "${S}"/sbin/htpdate

    if use networkmanager; then
	    insinto /etc/NetworkManager/dispatcher.d/
        doins "${FILESDIR}/20-time.sh"
    fi
}