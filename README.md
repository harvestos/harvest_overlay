Harvest: Overlay for Gentoo Linux
-------------------------------------

## How to install the overlay
You can clone the repository and create `/etc/portage/repos.conf/harvest.conf`
with the following contents:
```ini
[harvest]
priority = 50
location = /usr/local/portage/harvest-overlay
auto-sync = yes
sync-type = git
sync-uri = https://gitlab.com/harvestos/harvest_overlay.git
```

Support profile desktop plasma/lxqt/dwm/i3wm

Desktop plasma and lxqt default use sddm and elogind

Desktop dwm and i3wm default slim

[Lxqt](https://gitlab.com/harvestos/harvest_overlay/tree/master/profiles/harvest/linux/amd64/1.0/desktop/lxqt)
[Plasma](https://gitlab.com/harvestos/harvest_overlay/tree/master/profiles/harvest/linux/amd64/1.0/desktop/plasma)
[i3wm](https://gitlab.com/harvestos/harvest_overlay/tree/master/profiles/harvest/linux/amd64/1.0/desktop/i3wm)
[dwm](https://gitlab.com/harvestos/harvest_overlay/tree/master/profiles/harvest/linux/amd64/1.0/desktop/dwm)